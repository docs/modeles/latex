# Recherche de tous les fichiers .tex
TEX := $(shell find . -type f -name "*.tex")

# Regroupe les noms des fichiers PDF qui seront générés à partir des fichiers LaTeX
PDF := $(TEX:.tex=.pdf)

# Sans argument à `make`, compile tous les PDF
all: $(PDF)

# Règle pour la compilation des fichiers LaTeX
%.pdf: %.tex
	cd $(@D) && pdflatex -interaction=errorstopmode $(<F)

